<?php

namespace Vendor\ElementProperty;

use Bitrix\Iblock\ElementTable;
use Bitrix\Iblock\PropertyTable;

/**
 * Class ElementPropertySTable - заготовка для DataManger
 */
abstract class ElementPropertySTable extends \Bitrix\Main\Entity\DataManager
{
    /**
     * @return array
     */
    public static function getMap(): array
    {
        return [
            'IBLOCK_ELEMENT_ID' => [
                'data_type' => 'integer',
                'primary'   => true,
            ],
            'IBLOCK_ELEMENT_BY' => [
                'data_type' => ElementTable::class,
                'reference' => [
                    '=this.IBLOCK_ELEMENT_ID' => 'ref.ID',
                ],
            ],
        ];
    }

    /**
     * Вернет набор свойств инфоблока в формате <PROPERTY_CODE> => <PROPERTY_ID>
     *
     * @param $iblockId
     *
     * @return array
     */
    protected static function getPropertyByIblock($iblockId): array
    {
        if (!$iblockId) {
            return [];
        }

        $arTmp = PropertyTable::query()
                              ->setSelect(['ID', 'CODE'])
                              ->addFilter('IBLOCK_ID', $iblockId)
                              ->fetchAll();

        $arProperty = [];
        foreach ($arTmp as $ar) {
            $arProperty[$ar['CODE']] = (int)$ar['ID'];
        }

        return $arProperty;
    }
}

