<?php

namespace Vendor\Thanks;

use Bitrix\Iblock\IblockTable;
use Bitrix\Iblock\SectionTable;
use Bitrix\Main\ORM\Fields\ExpressionField;
use Bitrix\Main\ORM\Query\Query;
use Bitrix\Main\Type\Date;

/**
 * Class Thanks - класс реализует выборку сущностей из ИБ
 *
 * Все Связи между таблицами реализованы через DataManager
 * @see \Vendor\Thanks\ElementPropertyThanksSTable -
 *
 * @package Vendor\Thanks
 */
class Thanks
{
    // Символьный код ИБ в котом будем искать свойства
    const IBLOCK_CODE = 'THANKS';

    //Код свойства отправителя благдарности для группировки
    const GROUP_AUTHOR = 'PROPERTY_USER_FROM';

    //Код свойства получателя благдарности для группировки
    const GROUP_RECIPIENT = 'PROPERTY_USER_TO';

    /**
     * @param array $filter
     * @param string $group
     * @param string $sort
     * @param int $limit
     *
     * @param int $offset
     *
     * @return array
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public static function getThanks(
        array $filter = [],
        string $group = '',
        string $sort = '',
        int $limit = 0,
        int $offset = 0
    ): array {
        /**
         * @var Query $thankPropQuery
         */
        $thankPropQuery = new Query(ElementPropertyThanksSTable::compileEntityByIblockCode(self::getIblockId()));
        $thankPropQuery
            ->addSelect('THANKS')
            ->addOrder('THANKS', $sort ?: 'DESC')
            ->addSelect($group ?: self::GROUP_RECIPIENT, 'USER')
            //Группировка подставит автоматом на основе поля THANKS
            ->registerRuntimeField('THANKS', new ExpressionField('THANKS', 'COUNT(*)'));

        if ($offset > 0) {
            $thankPropQuery->setOffset($offset);
        }
        if ($limit > 0) {
            $thankPropQuery->setLimit($limit);
        }

        if ($filter['DEPARTMENT']) {
            /**
             * Фильтрация по департаменту реализованна через таблицу b_user_index
             * @see \Bitrix\Main\UserIndexTable
             */
            $thankPropQuery->addFilter(self::GROUP_RECIPIENT . '_BY.INDEX.UF_DEPARTMENT_NAME', $filter['DEPARTMENT']);
        }
        if ($filter['DATE_CREATE']) {
            $thankPropQuery->whereBetween(
                'IBLOCK_ELEMENT_BY.DATE_CREATE',
                Date::createFromText($filter['DATE_CREATE'] . ' 00:00:00'),
                Date::createFromText($filter['DATE_CREATE'] . ' 23:23:59')
            );
        }
        if ($filter['AUTHOR_ID']) {
            $thankPropQuery->addFilter('PROPERTY_USER_FROM_BY.ID', $filter['AUTHOR_ID']);
        }
        if ($filter['RECIPIENT_ID']) {
            $thankPropQuery->addFilter('PROPERTY_USER_TO_BY.ID', $filter['RECIPIENT_ID']);
        }

        //dump($thankPropQuery->getSelect());
        //dump($thankPropQuery->getOrder());
        //dump($thankPropQuery->getFilter());

        return $thankPropQuery->fetchAll();
    }

    /**
     * @return int
     */
    private static function getIblockId(): int
    {
        return (int)IblockTable::query()
                               ->setSelect(['ID'])->addFilter('CODE', self::IBLOCK_CODE)
                               ->fetch()['ID'];
    }

    /**
     * @return array
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public static function getDepartment(): array
    {
        return (new Query(SectionTable::class))
            ->setSelect(['DEPTH_LEVEL', 'ID', 'NAME'])
            ->addFilter('ACTIVE', 'Y')
            ->addFilter('IBLOCK.CODE', 'departments')
            ->exec()->fetchAll();
    }
}