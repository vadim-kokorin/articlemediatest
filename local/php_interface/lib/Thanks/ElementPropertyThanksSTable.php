<?php

namespace Vendor\Thanks;

use Bitrix\Main\Entity\ReferenceField;
use Bitrix\Main\UserTable;
use Vendor\ElementProperty\ElementPropertySTable;

/**
 * Class ElementPropertySTable - класс реализующий таблицу с отдельными свойствами для каждого ИБ
 */
class ElementPropertyThanksSTable extends ElementPropertySTable
{
    private static $entityInstance = [];

    /**
     * ElementPropertyThanksSTable constructor.
     */
    public function __construct()
    {
        throw new \RangeException('Используйте метод compileEntityByIblockCode для компиляции класса нужного ИБ');
    }

    /**
     * Скомпилирует и вернет класс DataManger под таблицу со свойствами нашего инфоблока
     *
     * Скомпилированный класс будет иметь все свойства ИБ благодарностей + join'ы с UserTable для каждого свойства
     * По дефолту считаю что там могут быть свойства только с привязкой к юзеру
     *
     * @param $iblockId
     *
     * @return mixed|null
     */
    final public static function compileEntityByIblockCode($iblockId)
    {
        $iblockId = (int)$iblockId;
        if ($iblockId === 0) {
            return null;
        }

        if (!isset(self::$entityInstance[$iblockId])) {
            $arProperty = self::getPropertyByIblock($iblockId);
            $className = 'ElementPropertyThanksS' . $iblockId . 'Table';
            $entityName = "\\Vendor\\Thanks\\" . $className;

            $mapFieldStr = '';
            foreach ($arProperty as $propCode => $propId) {
                $fullPropCode = 'PROPERTY_' . $propCode;
                $mapFieldStr .= sprintf(
                    '$fields["%s"] = new \Bitrix\Main\Entity\IntegerField("PROPERTY_%s");',
                    $fullPropCode,
                    $propId
                );
                $mapFieldStr .= sprintf(
                    '$fields["%s_BY"] = new \%s("%s_BY", "\%s", ["this.%s"=>"ref.ID"]);',
                    $fullPropCode,
                    ReferenceField::class,
                    $fullPropCode,
                    UserTable::class,
                    $fullPropCode
                );
            }

            $entity = '
			namespace Vendor\Thanks;
			class ' . $className . ' extends \Vendor\ElementProperty\ElementPropertySTable
			{
                public static function getTableName(): string
                {
                    return "b_iblock_element_prop_s' . $iblockId . '";
                }
    
                public static function getMap(): array
				{
                    $fields = parent::getMap();
                    ' . $mapFieldStr . '
                    
                    return $fields; 
                }
			}';
            eval($entity);
            self::$entityInstance[$iblockId] = $entityName;
        }

        return self::$entityInstance[$iblockId];
    }
}