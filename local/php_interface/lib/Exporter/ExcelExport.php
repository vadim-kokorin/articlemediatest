<?php

namespace Vendor\Exporter;

use Bitrix\Main\Application;
use Bitrix\Main\NotImplementedException;

/**
 * Class ExcelExport
 * @package Vendor\Exporter
 */
class ExcelExport extends Exporter
{
    /**
     * @return self
     */
    public function exec(): self
    {
        if (!$this->items) {
            return $this;
        }
        $this->data = '
		<html>
		<head>
		<meta http-equiv="Content-Type" content="text/html; charset=' . LANG_CHARSET . '">
		<style>
			td {mso-number-format:\@;}
			.number0 {mso-number-format:0;}
			.number2 {mso-number-format:Fixed;}
		</style>
		</head>
		<body>';

        $this->data .= '<table border="1">';

        foreach ($this->items as $arItem) {
            $this->data .= '<tr><td>';
            $this->data .= implode('</td><td>', $arItem);
            $this->data .= '</td></tr>';
        }

        $this->data .= '</table>';
        $this->data .= '</body></html>';

        return $this;
    }

    /**
     * Отдать контент в браузер
     * @throws \Bitrix\Main\ArgumentNullException
     * @throws \Bitrix\Main\ArgumentOutOfRangeException
     * @throws \Bitrix\Main\SystemException
     */
    public function toOutput()
    {
        global $APPLICATION;

        $APPLICATION->RestartBuffer();
        Application::getInstance()
                   ->getContext()
                   ->getResponse()
                   ->addHeader('Content-Type', 'application/vnd.ms-excel')
                   ->addHeader('Content-Disposition', 'attachment;filename=user-list.xls');

        echo $this->data;

        require $_SERVER['DOCUMENT_ROOT'] . BX_ROOT . '/modules/main/include/epilog_after.php';
        die();
    }

    /**
     * @param string $fileName
     *
     * @throws NotImplementedException
     */
    public function toFile(string $fileName)
    {
        throw new NotImplementedException('Не реализовано');
    }
}