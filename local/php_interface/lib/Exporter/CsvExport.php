<?php

namespace Vendor\Exporter;

use Bitrix\Main\Application;
use Bitrix\Main\NotImplementedException;

class CsvExport extends Exporter
{
    /**
     * @return self
     */
    public function exec(): self
    {
        if (!$this->items) {
            return $this;
        }

        $this->data = '';
        //        $this->data .= implode(';', array_keys($this->items[0]));
        //        $this->data .= PHP_EOL;
        foreach ($this->items as $arItem) {
            $this->data .= implode(';', $arItem);
            $this->data .= PHP_EOL;
        }

        return $this;
    }

    /**
     * Отдать контент в браузер
     * @throws \Bitrix\Main\ArgumentNullException
     * @throws \Bitrix\Main\ArgumentOutOfRangeException
     * @throws \Bitrix\Main\SystemException
     */
    public function toOutput()
    {
        global $APPLICATION;

        $APPLICATION->RestartBuffer();
        $response = Application::getInstance()->getContext()->getResponse();
        $response->addHeader('Content-Type', 'text/csv')
                 ->addHeader('Content-Disposition', 'attachment;filename=user-list.csv');

        echo iconv('UTF-8', 'CP1251', $this->data);

        require $_SERVER['DOCUMENT_ROOT'] . BX_ROOT . '/modules/main/include/epilog_admin_after.php';

        die();
    }

    /**
     * @param string $fileName
     *
     * @throws NotImplementedException
     */
    public function toFile(string $fileName)
    {
        throw new NotImplementedException('Не реализовано');
    }
}