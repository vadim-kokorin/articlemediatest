<?php

namespace Vendor\Component\Traits;

/**
 * Trait UseCachePlan
 * @package Vendor\Component\Traits
 */
trait UseCachePlan
{
    /**
     * @var string Тип плана кеширования компонента
     */
    private $cachePlan = 'one_hour';

    /**
     * Применяет план кеширования к параметрам компонента: задаёт тип, время жизни и группы кеширования.
     *
     * @param array $arParams Массив входных параметров.
     *
     * @return self Изменённый массив входных параметров.
     */
    private function prepareCachePlanParams(&$arParams): self
    {
        $arParams['CACHE_TYPE'] = $arParams['CACHE_TYPE'] ?? 'Y';
        $arParams['CACHE_GROUPS'] = $arParams['CACHE_GROUPS'] ?? 'N';

        switch ($this->cachePlan) {
            /** План кеширования "Почасовой" */
            case 'one_hour':
                $arParams['CACHE_TIME'] = $arParams['CACHE_TIME'] ?? 3600;

                break;

            /** План кеширования "Сутки" */
            case 'one_day':
                $arParams['CACHE_TIME'] = $arParams['CACHE_TIME'] ?? 86400;//(24 * 60 * 60);

                break;

            /** План кеширования "Месяц" */
            case 'one_month':
                $arParams['CACHE_TIME'] = $arParams['CACHE_TIME'] ?? 2592000; //(30 * 24 * 60 * 60);

                break;

            /** План кеширования "Год" */
            case 'one_year':
                $arParams['CACHE_TIME'] = $arParams['CACHE_TIME'] ?? 31536000;//(365 * 24 * 60 * 60);

                break;
        }

        return $this;
    }
}