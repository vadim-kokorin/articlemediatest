<?php

namespace Vendor\Component\Traits;

use Bitrix\Main\Error;
use Bitrix\Main\ErrorCollection;

/**
 * Trait UseErrorCollectionTrait
 * @package Vendor\Component\Traits
 */
trait UseErrorCollectionTrait
{
    /**
     * @var ErrorCollection
     */
    protected $errorCollection;

    /**
     * Get a collection.
     * @return ErrorCollection
     */
    public function getErrorCollection(): ErrorCollection
    {
        if ($this->errorCollection instanceof ErrorCollection) {
            return $this->errorCollection;
        }

        $this->errorCollection = new ErrorCollection();

        return $this->errorCollection;
    }

    /**
     * @param string|array $txt
     *
     * @param string $prefix
     *
     * @return self
     */
    public function addError($txt, $prefix = ''): self
    {
        $arTxt = (array)$txt;
        foreach ($arTxt as $v) {
            $this->getErrorCollection()->add([new Error($prefix . $v)]);
        }

        return $this;
    }

    /**
     * Выведет все ошибки битриксовым методов ShowError
     *
     * @param string $format
     *
     * @return self
     */
    public function showAllErrors($format = 'ShowError'): self
    {
        foreach ($this->getErrorCollection() as $error) {
            /** @var Error $error */
            if ($format === 'ShowError') {
                ShowError($error->getMessage());
            } elseif ($format === 'br') {
                echo $error->getMessage() . '<br>';
            } else {
                echo $error->getMessage();
            }
        }

        return $this;
    }
}