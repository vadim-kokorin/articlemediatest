<?php

namespace Sprint\Migration;

use Vendor\Enum\IblockCode;

class Add_test_iblock20181223182336 extends Version
{

    protected $description = "";

    /**
     * @return bool
     */
    public function up()
    {
        $helper = new HelperManager();

        $helper->Iblock()->saveIblockType(
            [
                'ID'               => 'test',
                'SECTIONS'         => 'Y',
                'EDIT_FILE_BEFORE' => '',
                'EDIT_FILE_AFTER'  => '',
                'IN_RSS'           => 'N',
                'SORT'             => '500',
                'LIST_MODE'        => 'S',
                'LANG'             =>
                    [
                        'ru' =>
                            [
                                'NAME'         => 'test',
                                'SECTION_NAME' => '',
                                'ELEMENT_NAME' => 'test',
                            ],
                        'en' =>
                            [
                                'NAME'         => 'test',
                                'SECTION_NAME' => '',
                                'ELEMENT_NAME' => 'test',
                            ],
                    ],
            ]
        );

        $iblockId = $helper->Iblock()->saveIblock(
            [
                'IBLOCK_TYPE_ID'     => 'test',
                'LID'                => 's1',
                'CODE'               => 'list_elements',
                'NAME'               => 'test',
                'ACTIVE'             => 'Y',
                'SORT'               => '500',
                'LIST_PAGE_URL'      => '#SITE_DIR#/list.php',
                'DETAIL_PAGE_URL'    => '#SITE_DIR#/list.php',
                'SECTION_PAGE_URL'   => null,
                'CANONICAL_PAGE_URL' => '#SITE_DIR#/list.php',
                'PICTURE'            => null,
                'DESCRIPTION'        => '',
                'DESCRIPTION_TYPE'   => 'text',
                'RSS_TTL'            => '24',
                'RSS_ACTIVE'         => 'Y',
                'RSS_FILE_ACTIVE'    => 'N',
                'RSS_FILE_LIMIT'     => null,
                'RSS_FILE_DAYS'      => null,
                'RSS_YANDEX_ACTIVE'  => 'N',
                'XML_ID'             => null,
                'INDEX_ELEMENT'      => 'N',
                'INDEX_SECTION'      => 'N',
                'WORKFLOW'           => 'N',
                'BIZPROC'            => 'N',
                'SECTION_CHOOSER'    => 'L',
                'LIST_MODE'          => '',
                'RIGHTS_MODE'        => 'S',
                'SECTION_PROPERTY'   => 'N',
                'PROPERTY_INDEX'     => 'N',
                'VERSION'            => '1',
                'LAST_CONV_ELEMENT'  => '0',
                'SOCNET_GROUP_ID'    => null,
                'EDIT_FILE_BEFORE'   => '',
                'EDIT_FILE_AFTER'    => '',
                'SECTIONS_NAME'      => null,
                'SECTION_NAME'       => null,
                'ELEMENTS_NAME'      => 'Элементы',
                'ELEMENT_NAME'       => 'Элемент',
                'EXTERNAL_ID'        => null,
                'LANG_DIR'           => '/',
                'SERVER_NAME'        => null,
            ]
        );

        $arSectionIds = [];
        foreach ($this->getDataSections() as $arFields) {
            $arSectionIds[] = $helper->Iblock()->addSection($iblockId, $arFields);
        }
        foreach ($this->getDataElements() as $arFields) {
            //Привяжем к случайно выбранному разделу
            $arFields['IBLOCK_SECTION_ID'] = $arSectionIds[array_rand($arSectionIds)];
            $helper->Iblock()->addElement($iblockId, $arFields);
            dump($arFields);
        }

        return true;
    }

    /**
     * @return bool
     */
    public function down()
    {
        $helper = new HelperManager();

        $helper->Iblock()->deleteIblockIfExists(IblockCode::LIST_ELEMENTS);

        return true;
    }

    /**
     * @return array
     */
    public function getDataElements(): array
    {
        return [
            ['NAME' => 'testnametest'],
            ['NAME' => 'test neme test'],
            ['NAME' => 'ne test'],
            ['NAME' => 'test2 name test'],
            ['NAME' => 'test name test'],
        ];
    }

    /**
     * @return array
     */
    public function getDataSections(): array
    {
        return [
            ['NAME' => 'test section 2'],
            ['NAME' => 'test section 1'],
        ];
    }

}
