<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php';

\WebArch\BitrixNeverInclude\BitrixNeverInclude::registerModuleAutoload();