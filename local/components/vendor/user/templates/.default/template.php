<?php

use Vendor\Components\UserList;

/**
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $templateFile
 * @var string $templateFolder
 * @var UserList $component
 * @var string $componentPath
 */
foreach ($component->getErrorCollection() as $error) {
    ShowError($error->getMessage());
}

//покажем панель с кнопками
$APPLICATION->IncludeComponent(
    'bitrix:main.interface.toolbar',
    '',
    [
        'BUTTONS' => [
            [
                'TEXT'  => 'Выгрузка в CSV',
                'TITLE' => 'Выгрузка в CSV',
                'LINK'  => $APPLICATION->GetCurPageParam('mode=csv'),
                'ICON'  => 'btn-settings',
            ],
            [
                'TEXT'  => 'Выгрузка в Excel',
                'TITLE' => 'Выгрузка в Excel',
                'LINK'  => $APPLICATION->GetCurPageParam('mode=excel'),
                'ICON'  => 'btn-excel',
            ],
            [
                'TEXT'  => 'Выгрузка в XML',
                'TITLE' => 'Выгрузка в XML',
                'LINK'  => $APPLICATION->GetCurPageParam('mode=xml'),
                'ICON'  => 'btn-settings',
            ],

        ],
    ],
    $component
);

$APPLICATION->IncludeComponent(
    'bitrix:main.ui.grid',
    '',
    [
        'GRID_ID'                   => 'users_grid',
        'HEADERS'                   => [
            ['id' => 'ID', 'name' => 'ID', 'default' => true],
            ['id' => 'EMAIL', 'name' => 'E-mail', 'default' => true],
        ],
        'ROWS'                      => array_map(
            function ($arItem) {
                return ['columns' => $arItem];
            },
            $component->getItems()
        ),
        'SHOW_SELECTED_COUNTER'     => false,
        'SHOW_TOTAL_COUNTER'        => true,
        'SHOW_ROW_CHECKBOXES'       => false,
        'SHOW_ACTION_PANEL'         => false,
        'SHOW_CHECK_ALL_CHECKBOXES' => false,
        'NAV_OBJECT'                => $component->getNavigation(),
        'NAV_PARAMS'                => [
            'SEF_MODE' => 'N',
        ],
        'TOTAL_ROWS_COUNT'          => $component->getNavigation()->getRecordCount(),
    ],
    $component
);