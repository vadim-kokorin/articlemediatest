<?php
/**
 *
 */

namespace Vendor\Components;

use Bitrix\Iblock\ElementTable;
use Bitrix\Main\ORM\Query\Query;
use CBitrixComponent;
use Exception;
use Vendor\Component\Traits\UseCachePlan;
use Vendor\Component\Traits\UseErrorCollectionTrait;
use Vendor\Enum\IblockCode;

/** @noinspection AutoloadingIssuesInspection */

/**
 * Class ListSimple
 * @package Vendor\Components
 */
class ListSimple extends CBitrixComponent
{
    use UseCachePlan, UseErrorCollectionTrait;

    /**
     * @param $arParams
     *
     * @return array
     */
    public function onPrepareComponentParams($arParams): array
    {
        $arParams['COUNT'] = (int)$arParams['COUNT'];
        $arParams['COUNT'] = $arParams['COUNT'] ?: 10;

        $arParams['QUERY'] = htmlspecialcharsbx($this->request->get('name'));

        /**
         * Apply default plan one_hour
         * @see \Vendor\Component\Traits\UseCachePlan::$cachePlan
         */
        $this->prepareCachePlanParams($arParams);

        return $arParams;
    }

    /**
     * @return void
     */
    public function executeComponent()
    {
        if ($this->startResultCache()) {
            $this->getItems();
            if (!$this->getErrorCollection()->isEmpty()) {
                $this->abortResultCache();
            }
            $this->includeComponentTemplate();
        }
    }

    /**
     * @return array
     */
    public function getItems(): array
    {
        /** @var Query $query */
        if ($this->arResult['ITEMS'] !== null) {
            return $this->arResult['ITEMS'];
        }

        $this->arResult['ITEMS'] = [];

        if ($this->arParams['QUERY']) {
            $query = ElementTable::query();
            $query->addSelect('NAME')
                  ->addSelect('IBLOCK_SECTION.NAME', 'SECTION_NAME')
                  ->addFilter('ACTIVE', 'Y')
                  ->addFilter('IBLOCK.CODE', IblockCode::LIST_ELEMENTS)
                  ->addFilter('NAME', '%' . $this->arParams['QUERY'] . '%')
                  ->setLimit($this->arParams['COUNT']);

            try {
                $this->arResult['ITEMS'] = $query->fetchAll();
            } catch (Exception $e) {
                $this->addError($e->getMessage());
            }
        }

        return $this->arResult['ITEMS'];
    }
}